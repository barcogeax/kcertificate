import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.barco.geax"
version = "1.0.0"

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.5.31"
    id("io.gitlab.arturbosch.detekt") version "1.19.0"
    id("org.jlleitschuh.gradle.ktlint") version "10.2.0"
    application
}

repositories {
    mavenLocal()
    mavenCentral()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        languageVersion = "1.5"
        apiVersion = "1.5"
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.compileJava {
    options.release.set(11)
}

detekt {
    toolVersion = "1.17.0"
    input = files(
        "src/main/kotlin",
        "src/test/kotlin"
    )
    parallel = true
    config = files("$rootDir/detekt-config.yml")
    buildUponDefaultConfig = true
    disableDefaultRuleSets = false
    debug = false
    ignoreFailures = false
    reports {
        xml {
            enabled = true
            destination = file("build/reports/detekt.xml")
        }
        html {
            enabled = true
            destination = file("build/reports/detekt.html")
        }
        txt {
            enabled = true
            destination = file("build/reports/detekt.txt")
        }
        sarif {
            enabled = false
        }
    }
}

dependencies {
    implementation("org.bouncycastle:bcpkix-jdk15on:1.69")
}
