package com.barco.geax.kcertificate

import org.bouncycastle.asn1.x500.X500Name
import javax.security.auth.x500.X500Principal

interface DistinguishedName {
    val x500Name: X500Name
    val x500Principal: X500Principal
    val encoded: ByteArray
    val name: String
}
