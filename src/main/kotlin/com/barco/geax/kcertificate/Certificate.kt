package com.barco.geax.kcertificate

import java.io.File
import java.security.PrivateKey
import java.security.cert.X509Certificate

interface Certificate {
    val x509Certificate: X509Certificate
    fun print(): String
    fun save(file: File)
    fun save(fileName: String)
    fun attachPrivateKey(privateKey: PrivateKey): CertificateWithPrivateKey
}
