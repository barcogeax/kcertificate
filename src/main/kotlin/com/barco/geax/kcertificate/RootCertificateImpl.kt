package com.barco.geax.kcertificate

import org.bouncycastle.cert.X509CertificateHolder
import java.io.IOException
import java.security.KeyPair
import java.security.PrivateKey
import java.security.cert.CertificateEncodingException
import java.security.cert.X509Certificate

internal class RootCertificateImpl(
    caCertificate: X509Certificate,
    caPrivateKey: PrivateKey
) : CertificateWithPrivateKeyImpl(caCertificate, caPrivateKey), RootCertificate {
    private var caCertificateHolder: X509CertificateHolder? = null
    override fun signCsr(request: CSR): Signer {
        val pair = KeyPair(x509Certificate.publicKey, privateKey)
        val signerSubject = CA.dn(caCertificateHolder!!.subject)
        return SignerImpl(pair, signerSubject, request.publicKey, request.subject)
    }

    companion object {
        const val KEYSTORE_TYPE = "PKCS12"
    }

    init {
        try {
            caCertificateHolder = X509CertificateHolder(caCertificate.encoded)
        } catch (e: CertificateEncodingException) {
            throw CaException(e)
        } catch (e: IOException) {
            throw CaException(e)
        }
    }
}
