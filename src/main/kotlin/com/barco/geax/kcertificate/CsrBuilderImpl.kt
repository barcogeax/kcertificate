package com.barco.geax.kcertificate

import com.barco.geax.kcertificate.KeysUtil.generateKeyPair
import org.bouncycastle.operator.OperatorCreationException
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder
import java.security.KeyPair

internal class CsrBuilderImpl : CsrBuilder {
    override fun generateRequest(name: DistinguishedName): CsrWithPrivateKey {
        val pair: KeyPair = generateKeyPair()
        return try {
            val privateKey = pair.private
            val publicKey = pair.public
            val x500Name = name.x500Name
            val signGen = JcaContentSignerBuilder(SIGNATURE_ALGORITHM)
                .build(privateKey)
            val builder: PKCS10CertificationRequestBuilder = JcaPKCS10CertificationRequestBuilder(
                x500Name, publicKey
            )
            val csr = builder.build(signGen)
            CsrWithPrivateKeyImpl(csr, privateKey)
        } catch (e: OperatorCreationException) {
            throw CaException(e)
        }
    }

    companion object {
        private const val SIGNATURE_ALGORITHM = "SHA256withECDSA"
    }
}
