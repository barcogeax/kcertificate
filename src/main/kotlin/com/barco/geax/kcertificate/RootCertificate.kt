package com.barco.geax.kcertificate

interface RootCertificate : CertificateWithPrivateKey {
    fun signCsr(request: CSR): Signer
}
