package com.barco.geax.kcertificate

interface CsrBuilder {
    fun generateRequest(name: DistinguishedName): CsrWithPrivateKey
}
