package com.barco.geax.kcertificate

import java.math.BigInteger
import java.security.SecureRandom

internal class SerialNumberGenerator @JvmOverloads constructor(
    private val random: SecureRandom = SecureRandom(),
    private val length: Int = DEFAULT_SERIAL_LENGTH
) {
    fun generateRandomSerialNumber(): BigInteger {
        return BigInteger(length, random)
    }

    companion object {
        private const val DEFAULT_SERIAL_LENGTH = 128
        var instance = SerialNumberGenerator()
    }
}
