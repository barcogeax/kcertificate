package com.barco.geax.kcertificate

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.security.KeyStore
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.security.cert.CertificateException

internal class KeyStoreReaderImpl : KeyStoreReader {
    override fun listAliases(keyStorePath: String, password: CharArray): List<String> {
        val file = File(keyStorePath)
        return listAliases(file, password)
    }

    override fun listAliases(keyStoreFile: File, password: CharArray): List<String> {
        return try {
            if (!keyStoreFile.exists() || !keyStoreFile.isFile) return ArrayList()
            val keyStore = KeyStore.getInstance(KEYSTORE_TYPE)
            FileInputStream(keyStoreFile).use { stream -> keyStore.load(stream, password) }
            listAliases(keyStore)
        } catch (e: KeyStoreException) {
            throw CaException(e)
        } catch (e: IOException) {
            throw CaException(e)
        } catch (e: CertificateException) {
            throw CaException(e)
        } catch (e: NoSuchAlgorithmException) {
            throw CaException(e)
        }
    }

    override fun listAliases(keyStore: KeyStore): List<String> {
        val res: MutableList<String> = ArrayList()
        return try {
            val aliases = keyStore.aliases()
            while (aliases.hasMoreElements()) {
                res.add(aliases.nextElement())
            }
            res
        } catch (e: KeyStoreException) {
            throw CaException(e)
        }
    }

    companion object {
        const val KEYSTORE_TYPE = "PKCS12"
    }
}
