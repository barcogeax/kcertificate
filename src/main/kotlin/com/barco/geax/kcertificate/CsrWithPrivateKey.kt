package com.barco.geax.kcertificate

import java.security.PrivateKey

interface CsrWithPrivateKey : CSR {
    val privateKey: PrivateKey
}
