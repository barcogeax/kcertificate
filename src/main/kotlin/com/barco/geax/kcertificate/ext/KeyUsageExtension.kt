package com.barco.geax.kcertificate.ext

import org.bouncycastle.asn1.x509.Extension
import org.bouncycastle.asn1.x509.KeyUsage

class KeyUsageExtension internal constructor(keyUsages: Int) :
    CertExtension(Extension.keyUsage, false, KeyUsage(keyUsages)) {
    internal constructor(vararg usages: KeyUsage) : this(getUsages(usages))

    enum class KeyUsage(val keyUsage: Int) {
        DIGITAL_SIGNATURE(org.bouncycastle.asn1.x509.KeyUsage.digitalSignature),
        NON_REPUDIATION(org.bouncycastle.asn1.x509.KeyUsage.nonRepudiation),
        KEY_ENCIPHERMENT(org.bouncycastle.asn1.x509.KeyUsage.keyEncipherment),
        DATA_ENCIPHERMENT(org.bouncycastle.asn1.x509.KeyUsage.dataEncipherment),
        KEY_AGREEMENT(org.bouncycastle.asn1.x509.KeyUsage.keyAgreement),
        KEY_CERT_SIGN(org.bouncycastle.asn1.x509.KeyUsage.keyCertSign),
        CRL_SIGN(org.bouncycastle.asn1.x509.KeyUsage.cRLSign),
        ENCIPHER_ONLY(org.bouncycastle.asn1.x509.KeyUsage.encipherOnly),
        DECIPHER_ONLY(org.bouncycastle.asn1.x509.KeyUsage.encipherOnly)
    }

    companion object {
        fun create(vararg usages: KeyUsage): KeyUsageExtension {
            return KeyUsageExtension(*usages)
        }

        private fun getUsages(usages: Array<out KeyUsage>): Int {
            var u = 0
            for (ku in usages) {
                u = u or ku.keyUsage
            }
            return u
        }
    }
}
