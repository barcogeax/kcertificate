package com.barco.geax.kcertificate.ext

import org.bouncycastle.asn1.ASN1Encodable
import org.bouncycastle.asn1.ASN1ObjectIdentifier

open class CertExtension(
    val oid: ASN1ObjectIdentifier,
    val isCritical: Boolean,
    val value: ASN1Encodable
) {
    override fun toString(): String {
        return "Extension [$oid=$value]"
    }
}
