package com.barco.geax.kcertificate.ext

import org.bouncycastle.asn1.x509.CRLDistPoint
import org.bouncycastle.asn1.x509.DistributionPoint
import org.bouncycastle.asn1.x509.DistributionPointName
import org.bouncycastle.asn1.x509.Extension
import org.bouncycastle.asn1.x509.GeneralNames
import org.bouncycastle.asn1.x509.ReasonFlags

/**
 * CRL Distribution Points
 */
class CrlDistPointExtension internal constructor(vararg points: DistributionPoint?) :
    CertExtension(Extension.cRLDistributionPoints, false, CRLDistPoint(points)) {
    companion object {
        /**
         * Creates a [CrlDistPointExtension] with only a `distributionPoint` URI (no `reasons`, no
         * `cRLIssuer` specified).
         */
        fun create(uri: String?): CrlDistPointExtension {
            return create(NameType.URI, uri)
        }

        /**
         * Creates a [CrlDistPointExtension] with only a `distributionPoint` [GeneralName] (no
         * `reasons`, no `cRLIssuer` specified).
         */
        fun create(type: NameType, name: String?): CrlDistPointExtension {
            return create(type, name, null, null, null)
        }

        fun create(
            distribPointNameType: NameType,
            distribPointName: String?,
            crlIssuerNameType: NameType?,
            crlIssuer: String?,
            reasons: ReasonFlags?
        ): CrlDistPointExtension {
            val dp = DistributionPointName(
                distribPointNameType.generalNames(distribPointName)
            )
            val crl: GeneralNames?
            crl = if (crlIssuerNameType != null && crlIssuer != null) {
                crlIssuerNameType.generalNames(crlIssuer)
            } else {
                null
            }
            return create(dp, reasons, crl)
        }

        fun create(
            distributionPoint: DistributionPointName?,
            reasons: ReasonFlags?,
            cRLIssuer: GeneralNames?
        ): CrlDistPointExtension {
            val p = DistributionPoint(distributionPoint, reasons, cRLIssuer)
            return create(p)
        }

        fun create(vararg points: DistributionPoint?): CrlDistPointExtension {
            return CrlDistPointExtension(*points)
        }
    }
}
