package com.barco.geax.kcertificate.ext

import org.bouncycastle.asn1.x509.ExtendedKeyUsage
import org.bouncycastle.asn1.x509.Extension
import org.bouncycastle.asn1.x509.KeyPurposeId

class ExtKeyUsageExtension internal constructor(extendedKeyUsage: ExtendedKeyUsage) :
    CertExtension(Extension.extendedKeyUsage, false, extendedKeyUsage) {

    internal constructor(usage: KeyPurposeId) : this(ExtendedKeyUsage(usage))
    internal constructor(usages: Array<out KeyPurposeId>) : this(ExtendedKeyUsage(usages))

    companion object {
        fun create(usage: KeyPurposeId): ExtKeyUsageExtension {
            return ExtKeyUsageExtension(usage)
        }

        fun create(vararg usages: KeyPurposeId): ExtKeyUsageExtension {
            return ExtKeyUsageExtension(usages)
        }
    }
}
