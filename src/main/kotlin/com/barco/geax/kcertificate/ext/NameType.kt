package com.barco.geax.kcertificate.ext

import org.bouncycastle.asn1.x509.GeneralName
import org.bouncycastle.asn1.x509.GeneralNames

enum class NameType(private val id: Int) {
    OTHER_NAME(GeneralName.otherName),
    RFC_822_NAME(GeneralName.rfc822Name),
    DNS_NAME(GeneralName.dNSName),
    X400_NAME(GeneralName.x400Address),
    DIRECTORY_NAME(GeneralName.directoryName),
    EDI_PARTY_NAME(GeneralName.ediPartyName),
    URI(GeneralName.uniformResourceIdentifier),
    IP_ADDRESS(GeneralName.iPAddress),
    REGISTERED_ID(GeneralName.registeredID);

    fun generalName(name: String?): GeneralName {
        return GeneralName(id, name)
    }

    fun generalNames(name: String?): GeneralNames {
        return GeneralNames(generalName(name))
    }
}
