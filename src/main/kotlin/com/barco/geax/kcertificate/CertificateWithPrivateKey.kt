package com.barco.geax.kcertificate

import java.io.File
import java.security.KeyStore
import java.security.PrivateKey

interface CertificateWithPrivateKey : Certificate {
    fun addToKeystore(keyStore: KeyStore, alias: String): KeyStore
    fun saveInPkcs12Keystore(alias: String): KeyStore
    fun exportPkcs12(
        keystorePath: String,
        keystorePassword: CharArray,
        alias: String
    )

    fun exportPkcs12(
        keystoreFile: File,
        keystorePassword: CharArray,
        alias: String
    )

    val privateKey: PrivateKey
    fun printKey(): String
    fun saveKey(file: File)
    fun saveKey(fileName: String)
}
