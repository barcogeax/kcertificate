package com.barco.geax.kcertificate

import com.barco.geax.kcertificate.ext.CertExtension
import org.bouncycastle.asn1.ASN1Encodable
import org.bouncycastle.asn1.ASN1ObjectIdentifier
import java.math.BigInteger
import java.time.ZonedDateTime

interface Signer {
    fun setSerialNumber(serialNumber: BigInteger): SignerWithSerial
    fun setRandomSerialNumber(): SignerWithSerial
    interface SignerWithSerial : Signer {
        fun sign(): Certificate
        fun setNotBefore(notBefore: ZonedDateTime): SignerWithSerial
        fun setNotAfter(notAfter: ZonedDateTime): SignerWithSerial
        fun validDuringYears(years: Int): SignerWithSerial
        fun addExtension(extension: CertExtension): SignerWithSerial
        fun addExtension(
            oid: ASN1ObjectIdentifier,
            isCritical: Boolean,
            value: ASN1Encodable
        ): SignerWithSerial
    }
}
