package com.barco.geax.kcertificate

interface CsrLoader {
    val csr: CSR
}
