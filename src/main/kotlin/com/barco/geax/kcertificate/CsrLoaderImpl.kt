package com.barco.geax.kcertificate

import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.pkcs.PKCS10CertificationRequest
import java.io.File
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.Files

internal class CsrLoaderImpl(private val file: File) : CsrLoader {
    constructor(fileName: String) : this(File(fileName))

    override val csr: CSR
        get() {
            try {
                Files.newBufferedReader(file.toPath(), StandardCharsets.UTF_8).use { pemReader ->
                    PEMParser(pemReader).use { pemParser ->
                        val parsedObj = pemParser.readObject()
                        return if (parsedObj is PKCS10CertificationRequest) {
                            CsrImpl(parsedObj)
                        } else throw CaException("Not a PKCS10CertificationRequest")
                    }
                }
            } catch (e: IOException) {
                throw CaException(e)
            }
        }
}
