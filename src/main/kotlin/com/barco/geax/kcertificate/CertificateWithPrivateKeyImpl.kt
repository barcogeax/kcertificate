package com.barco.geax.kcertificate

import org.bouncycastle.openssl.jcajce.JcaPEMWriter
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.io.StringWriter
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.StandardOpenOption
import java.security.KeyStore
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.security.PrivateKey
import java.security.cert.Certificate
import java.security.cert.CertificateException
import java.security.cert.X509Certificate

internal open class CertificateWithPrivateKeyImpl(
    certificate: X509Certificate,
    override val privateKey: PrivateKey
) : CertificateImpl(certificate), CertificateWithPrivateKey {

    override fun addToKeystore(keyStore: KeyStore, alias: String): KeyStore {
        return try {
            val certificate = x509Certificate
            val chain = arrayOf<Certificate>(certificate)
            keyStore.setKeyEntry(alias, privateKey, null, chain)
            keyStore
        } catch (e: KeyStoreException) {
            throw CaException(e)
        }
    }

    override fun saveInPkcs12Keystore(alias: String): KeyStore {
        return try {
            // init keystore
            val keyStore = KeyStore.getInstance(KEYSTORE_TYPE)
            keyStore.load(null, null)
            addToKeystore(keyStore, alias)
            keyStore
        } catch (e: KeyStoreException) {
            throw CaException(e)
        } catch (e: NoSuchAlgorithmException) {
            throw CaException(e)
        } catch (e: CertificateException) {
            throw CaException(e)
        } catch (e: IOException) {
            throw CaException(e)
        }
    }

    override fun exportPkcs12(
        keystorePath: String,
        keystorePassword: CharArray,
        alias: String
    ) {
        val file = File(keystorePath)
        exportPkcs12(file, keystorePassword, alias)
    }

    override fun exportPkcs12(
        keystoreFile: File,
        keystorePassword: CharArray,
        alias: String
    ) {
        try {
            val keyStore: KeyStore
            if (keystoreFile.exists() && keystoreFile.isFile) {
                // Load existing keystore
                keyStore = KeyStore.getInstance(KEYSTORE_TYPE)
                FileInputStream(keystoreFile).use { stream -> keyStore.load(stream, keystorePassword) }
                addToKeystore(keyStore, alias)
            } else {
                keyStore = saveInPkcs12Keystore(alias)
            }
            FileOutputStream(keystoreFile).use { stream -> keyStore.store(stream, keystorePassword) }
        } catch (e: KeyStoreException) {
            throw CaException(e)
        } catch (e: IOException) {
            throw CaException(e)
        } catch (e: CertificateException) {
            throw CaException(e)
        } catch (e: NoSuchAlgorithmException) {
            throw CaException(e)
        }
    }

    override fun printKey(): String {
        val sw = StringWriter()
        try {
            JcaPEMWriter(sw).use { writer ->
                writer.writeObject(privateKey)
                writer.flush()
                return sw.toString()
            }
        } catch (e: IOException) {
            throw CaException(e)
        }
    }

    override fun saveKey(file: File) {
        try {
            Files.newBufferedWriter(
                file.toPath(), StandardCharsets.UTF_8,
                StandardOpenOption.CREATE
            ).use { fw ->
                JcaPEMWriter(fw).use { writer ->
                    writer.writeObject(privateKey)
                    writer.flush()
                }
            }
        } catch (e: IOException) {
            throw CaException(e)
        }
    }

    override fun saveKey(fileName: String) {
        val file = File(fileName)
        saveKey(file)
    }

    companion object {
        const val KEYSTORE_TYPE = "PKCS12"
    }
}
