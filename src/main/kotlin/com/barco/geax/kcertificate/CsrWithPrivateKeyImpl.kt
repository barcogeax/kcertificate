package com.barco.geax.kcertificate

import org.bouncycastle.pkcs.PKCS10CertificationRequest
import java.security.PrivateKey

internal class CsrWithPrivateKeyImpl(
    request: PKCS10CertificationRequest,
    override val privateKey: PrivateKey
) : CsrImpl(request), CsrWithPrivateKey
