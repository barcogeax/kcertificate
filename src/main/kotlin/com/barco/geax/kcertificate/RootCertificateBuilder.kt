package com.barco.geax.kcertificate

import java.time.ZonedDateTime

interface RootCertificateBuilder {
    fun setNotBefore(notBefore: ZonedDateTime): RootCertificateBuilder
    fun setNotAfter(notAfter: ZonedDateTime): RootCertificateBuilder
    fun validDuringYears(years: Int): RootCertificateBuilder
    fun setCrlUri(crlUri: String?): RootCertificateBuilder
    fun build(): RootCertificate
}
