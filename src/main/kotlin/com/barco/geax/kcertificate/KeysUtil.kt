package com.barco.geax.kcertificate

import org.bouncycastle.jce.ECNamedCurveTable
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.jce.spec.ECParameterSpec
import java.security.InvalidParameterException
import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.Security

internal object KeysUtil {
    private const val ALGORITHM = "ECDSA"
    @JvmOverloads
    fun generateKeyPair(): KeyPair {
        if (Security.getProviders().firstOrNull() { it.name == BouncyCastleProvider.PROVIDER_NAME } == null) {
            Security.addProvider(BouncyCastleProvider())
        }
        return try {
            val ecSpec: ECParameterSpec = ECNamedCurveTable.getParameterSpec("P-256")
            val gen = KeyPairGenerator.getInstance(ALGORITHM)
            gen.initialize(ecSpec, SecureRandom())
            gen.generateKeyPair()
        } catch (e: NoSuchAlgorithmException) {
            throw CaException(e)
        } catch (e: InvalidParameterException) {
            throw CaException(e)
        }
    }
}
