package com.barco.geax.kcertificate

import org.bouncycastle.openssl.jcajce.JcaPEMWriter
import java.io.File
import java.io.IOException
import java.io.StringWriter
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.StandardOpenOption
import java.security.PrivateKey
import java.security.cert.X509Certificate

internal open class CertificateImpl(override val x509Certificate: X509Certificate) : Certificate {
    override fun print(): String {
        val sw = StringWriter()
        try {
            JcaPEMWriter(sw).use { writer ->
                writer.writeObject(x509Certificate)
                writer.flush()
                return sw.toString()
            }
        } catch (e: IOException) {
            throw CaException(e)
        }
    }

    override fun save(file: File) {
        try {
            Files.newBufferedWriter(
                file.toPath(), StandardCharsets.UTF_8,
                StandardOpenOption.CREATE
            ).use { fw ->
                JcaPEMWriter(fw).use { writer ->
                    writer.writeObject(x509Certificate)
                    writer.flush()
                }
            }
        } catch (e: IOException) {
            throw CaException(e)
        }
    }

    override fun save(fileName: String) {
        val file = File(fileName)
        save(file)
    }

    override fun attachPrivateKey(privateKey: PrivateKey): CertificateWithPrivateKey {
        return CertificateWithPrivateKeyImpl(x509Certificate, privateKey)
    }
}
