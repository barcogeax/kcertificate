package com.barco.geax.kcertificate

import org.bouncycastle.asn1.x500.X500NameBuilder
import org.bouncycastle.asn1.x500.style.BCStyle

internal class DnBuilderImpl : DnBuilder {
    private val builder: X500NameBuilder = X500NameBuilder()

    override fun setCn(cn: String): DnBuilder {
        builder.addRDN(BCStyle.CN, cn)
        return this
    }

    override fun setCommonName(cn: String): DnBuilder {
        return setCn(cn)
    }

    override fun setL(l: String): DnBuilder {
        builder.addRDN(BCStyle.L, l)
        return this
    }

    override fun setLocalityName(l: String): DnBuilder {
        return setL(l)
    }

    override fun setSt(st: String): DnBuilder {
        builder.addRDN(BCStyle.ST, st)
        return this
    }

    override fun setStateOrProvinceName(st: String): DnBuilder {
        return setSt(st)
    }

    override fun setO(o: String): DnBuilder {
        builder.addRDN(BCStyle.O, o)
        return this
    }

    override fun setOrganizationName(o: String): DnBuilder {
        return setO(o)
    }

    override fun setOu(ou: String): DnBuilder {
        builder.addRDN(BCStyle.OU, ou)
        return this
    }

    override fun setOrganizationalUnitName(ou: String): DnBuilder {
        return setOu(ou)
    }

    override fun setC(c: String): DnBuilder {
        builder.addRDN(BCStyle.C, c)
        return this
    }

    override fun setCountryName(c: String): DnBuilder {
        return setC(c)
    }

    override fun setStreet(street: String): DnBuilder {
        builder.addRDN(BCStyle.STREET, street)
        return this
    }

    override fun build(): DistinguishedName {
        val name = builder.build()
        return BcX500NameDnImpl(name)
    }
}
