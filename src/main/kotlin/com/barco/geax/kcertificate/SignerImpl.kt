package com.barco.geax.kcertificate

import com.barco.geax.kcertificate.Signer.SignerWithSerial
import com.barco.geax.kcertificate.ext.CertExtension
import org.bouncycastle.asn1.ASN1Encodable
import org.bouncycastle.asn1.ASN1ObjectIdentifier
import org.bouncycastle.asn1.x509.Extension
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo
import org.bouncycastle.cert.CertIOException
import org.bouncycastle.cert.X509v3CertificateBuilder
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils
import org.bouncycastle.operator.OperatorCreationException
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import java.math.BigInteger
import java.security.InvalidKeyException
import java.security.KeyPair
import java.security.NoSuchAlgorithmException
import java.security.NoSuchProviderException
import java.security.PublicKey
import java.security.SignatureException
import java.security.cert.CertificateException
import java.time.ZonedDateTime
import java.util.Date

internal class SignerImpl(
    private val signerKeyPair: KeyPair,
    private val signerDn: DistinguishedName,
    private val publicKey: PublicKey,
    private val dn: DistinguishedName
) : Signer, SignerWithSerial {

    private val extensions: MutableList<CertExtension> = ArrayList()
    private var serialNumber: BigInteger = BigInteger.valueOf(0)
    private var notBefore = ZonedDateTime.now()
    private var notAfter = notBefore.plusYears(1)
    override fun setSerialNumber(serialNumber: BigInteger): SignerWithSerial {
        this.serialNumber = serialNumber
        return this
    }

    override fun setRandomSerialNumber(): SignerWithSerial {
        serialNumber = CA.generateRandomSerialNumber()
        return this
    }

    override fun setNotBefore(notBefore: ZonedDateTime): SignerWithSerial {
        this.notBefore = notBefore
        return this
    }

    override fun setNotAfter(notAfter: ZonedDateTime): SignerWithSerial {
        this.notAfter = notAfter
        return this
    }

    override fun validDuringYears(years: Int): SignerWithSerial {
        notAfter = notBefore.plusYears(years.toLong())
        return this
    }

    override fun addExtension(extension: CertExtension): SignerWithSerial {
        extensions.add(extension)
        return this
    }

    override fun addExtension(
        oid: ASN1ObjectIdentifier,
        isCritical: Boolean,
        value: ASN1Encodable
    ): SignerWithSerial {
        extensions.add(CertExtension(oid, isCritical, value))
        return this
    }

    override fun sign(): Certificate {
        return try {
            val sigGen = JcaContentSignerBuilder(SIGNATURE_ALGORITHM)
                .build(signerKeyPair.private)
            val subPubKeyInfo = SubjectPublicKeyInfo.getInstance(
                publicKey.encoded
            )
            val extUtils = JcaX509ExtensionUtils()
            val certBuilder = X509v3CertificateBuilder(
                signerDn.x500Name,
                serialNumber,
                Date.from(notBefore.toInstant()),
                Date.from(notAfter.toInstant()),
                dn.x500Name,
                subPubKeyInfo
            )
                .addExtension(
                    Extension.authorityKeyIdentifier, false,
                    extUtils.createAuthorityKeyIdentifier(signerKeyPair.public)
                )
                .addExtension(
                    Extension.subjectKeyIdentifier, false,
                    extUtils.createSubjectKeyIdentifier(publicKey)
                )
            for (e in extensions) {
                certBuilder.addExtension(e.oid, e.isCritical, e.value)
            }
            val holder = certBuilder.build(sigGen)
            val cert = JcaX509CertificateConverter()
                .getCertificate(holder)
            cert.checkValidity()
            cert.verify(signerKeyPair.public)
            CertificateImpl(cert)
        } catch (e: OperatorCreationException) {
            throw CaException(e)
        } catch (e: CertificateException) {
            throw CaException(e)
        } catch (e: InvalidKeyException) {
            throw CaException(e)
        } catch (e: NoSuchAlgorithmException) {
            throw CaException(e)
        } catch (e: NoSuchProviderException) {
            throw CaException(e)
        } catch (e: SignatureException) {
            throw CaException(e)
        } catch (e: CertIOException) {
            throw CaException(e)
        }
    }

    companion object {
        private const val SIGNATURE_ALGORITHM = "SHA256withECDSA"
    }
}
