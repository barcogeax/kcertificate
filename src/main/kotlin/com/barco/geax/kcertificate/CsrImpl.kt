package com.barco.geax.kcertificate

import org.bouncycastle.openssl.PEMException
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import org.bouncycastle.pkcs.PKCS10CertificationRequest
import java.security.PublicKey

internal open class CsrImpl(request: PKCS10CertificationRequest) : CSR {
    final override val subject: DistinguishedName
    final override var publicKey: PublicKey

    init {
        subject = BcX500NameDnImpl(request.subject)
        publicKey = try {
            JcaPEMKeyConverter().getPublicKey(request.subjectPublicKeyInfo)
        } catch (e: PEMException) {
            throw CaException(e)
        }
    }
}
