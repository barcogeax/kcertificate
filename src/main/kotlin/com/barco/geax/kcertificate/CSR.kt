package com.barco.geax.kcertificate

import java.security.PublicKey

interface CSR {
    val subject: DistinguishedName
    val publicKey: PublicKey
}
