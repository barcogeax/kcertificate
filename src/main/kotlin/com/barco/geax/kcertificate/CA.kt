package com.barco.geax.kcertificate

import org.bouncycastle.asn1.x500.X500Name
import java.io.File
import java.math.BigInteger
import java.security.KeyStore
import javax.security.auth.x500.X500Principal

/**
 * Root of the *Certification Authority* DSL.
 *
 *
 *
 * All methods in this class are static. They should be statically imported:
 *
 *
 *
 * `import static com.barco.geax.kcertificate.CA.*;`
 *
 */
object CA {
    /**
     * Creates a builder object used to create a new self-signed root certificate.
     *
     * @param subject
     * Subject's Distinguished Name
     * @return a builder object
     */
    fun createSelfSignedCertificate(
        subject: DistinguishedName
    ): RootCertificateBuilder {
        return RootCertificateBuilderImpl(subject)
    }

    /**
     * Loads an existing [RootCertificate] from a `PKCS12` keystore.
     *
     * @param keystorePath
     * path of the PKCS12 keystore
     * @param password
     * password of the keystore
     * @param alias
     * Root certificate alias in the keystore
     * @return the loaded [RootCertificate]
     */
    fun loadRootCertificate(
        keystorePath: String,
        password: CharArray,
        alias: String
    ): RootCertificate {
        return RootCertificateLoader.loadRootCertificate(keystorePath, password, alias)
    }

    /**
     * Loads an existing [RootCertificate] from a `PKCS12` keystore.
     *
     * @param keystoreFile
     * PKCS12 keystore file
     * @param password
     * password of the keystore
     * @param alias
     * Root certificate alias in the keystore
     * @return the loaded [RootCertificate]
     */
    fun loadRootCertificate(
        keystoreFile: File,
        password: CharArray,
        alias: String
    ): RootCertificate {
        return RootCertificateLoader.loadRootCertificate(keystoreFile, password, alias)
    }

    /**
     * Loads an existing [RootCertificate] from a `PKCS12` keystore.
     *
     * @param keystoreFile
     * PKCS12 keystore, already "loaded"
     * @param alias
     * Root certificate alias in the keystore
     * @return the loaded [RootCertificate]
     */
    fun loadRootCertificate(
        keystore: KeyStore,
        alias: String
    ): RootCertificate {
        return RootCertificateLoader.loadRootCertificate(keystore, alias)
    }

    fun readKeystore(): KeyStoreReader {
        return KeyStoreReaderImpl()
    }

    /**
     * Creates a builder object used to create a new [CSR] (Certificate Signing Request).
     *
     * @return a builder object
     * @see CSR
     */
    fun createCsr(): CsrBuilder {
        return CsrBuilderImpl()
    }

    /**
     * Loads a [CSR] (Certificate Signing Request) from a file.
     *
     * @param csrFile
     * CSR file
     * @return the CSR object
     */
    fun loadCsr(csrFile: File): CsrLoader {
        return CsrLoaderImpl(csrFile)
    }

    /**
     * Loads a [CSR] (Certificate Signing Request) from a file.
     *
     * @param csrFileName
     * CSR file path
     * @return the CSR object
     */
    fun loadCsr(csrFileName: String): CsrLoader {
        return CsrLoaderImpl(csrFileName)
    }

    /**
     * Creates a builder object used to build a [DistinguishedName].
     *
     * @return a builder object
     * @see DistinguishedName
     */
    fun dn(): DnBuilder {
        return DnBuilderImpl()
    }

    /**
     * Builds a [DistinguishedName] from a [String].
     *
     * @param name
     * @return the [DistinguishedName] object
     */
    fun dn(name: String): DistinguishedName {
        return BcX500NameDnImpl(name)
    }

    /**
     * Builds a [DistinguishedName] from a Bouncy Castle [X500Name] object.
     *
     * @param name
     * @return the [DistinguishedName] object
     */
    fun dn(name: X500Name): DistinguishedName {
        return BcX500NameDnImpl(name)
    }

    /**
     * Builds a [DistinguishedName] from a [javax.security.auth.x500.X500Principal].
     *
     * @param name
     * @return the [DistinguishedName] object
     */
    fun dn(principal: X500Principal): DistinguishedName {
        return BcX500NameDnImpl(principal)
    }

    fun generateRandomSerialNumber(): BigInteger {
        return SerialNumberGenerator.Companion.instance.generateRandomSerialNumber()
    }
}
