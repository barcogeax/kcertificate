package com.barco.geax.kcertificate

import org.bouncycastle.asn1.x500.X500Name
import java.io.IOException
import javax.security.auth.x500.X500Principal

internal class BcX500NameDnImpl : DistinguishedName {
    override val x500Name: X500Name

    constructor(name: X500Name) {
        x500Name = name
    }

    constructor(name: String) {
        x500Name = X500Name(name)
    }

    constructor(principal: X500Principal) {
        x500Name = X500Name.getInstance(principal.encoded)
    }

    override val x500Principal: X500Principal
        get() = try {
            X500Principal(x500Name.encoded)
        } catch (e: IOException) {
            throw CaException(e)
        }
    override val encoded: ByteArray
        get() = try {
            x500Name.encoded
        } catch (e: IOException) {
            throw CaException(e)
        }
    override val name: String
        get() = x500Name.toString()

    override fun toString(): String {
        return name
    }
}
