package com.barco.geax.kcertificate

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.security.GeneralSecurityException
import java.security.KeyStore
import java.security.PrivateKey
import java.security.cert.X509Certificate

internal object RootCertificateLoader {
    fun loadRootCertificate(keystorePath: String, password: CharArray, alias: String): RootCertificateImpl {
        val file = File(keystorePath)
        return loadRootCertificate(file, password, alias)
    }

    fun loadRootCertificate(keystoreFile: File, password: CharArray, alias: String): RootCertificateImpl {
        try {
            val keystore = KeyStore.getInstance(RootCertificateImpl.KEYSTORE_TYPE)
            FileInputStream(keystoreFile).use { stream ->
                keystore.load(stream, password)
                return loadRootCertificate(keystore, alias)
            }
        } catch (e: GeneralSecurityException) {
            throw CaException(e)
        } catch (e: IOException) {
            throw CaException(e)
        }
    }

    fun loadRootCertificate(keystore: KeyStore, alias: String, keyPassword: String? = null): RootCertificateImpl {
        return try {
            val certificate = keystore.getCertificate(alias)
            val privateKey = keystore.getKey(alias, keyPassword?.toCharArray()) as PrivateKey
            if (certificate == null) throw CaException("Keystore does not contain certificate and key for alias $alias")
            RootCertificateImpl(certificate as X509Certificate, privateKey)
        } catch (e: GeneralSecurityException) {
            throw CaException(e)
        }
    }
}
