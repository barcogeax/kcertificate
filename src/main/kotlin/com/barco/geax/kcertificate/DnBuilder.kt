package com.barco.geax.kcertificate

interface DnBuilder {
    fun setCn(cn: String): DnBuilder
    fun setCommonName(cn: String): DnBuilder
    fun setL(l: String): DnBuilder
    fun setLocalityName(l: String): DnBuilder
    fun setSt(st: String): DnBuilder
    fun setStateOrProvinceName(st: String): DnBuilder
    fun setO(o: String): DnBuilder
    fun setOrganizationName(o: String): DnBuilder
    fun setOu(ou: String): DnBuilder
    fun setOrganizationalUnitName(ou: String): DnBuilder
    fun setC(c: String): DnBuilder
    fun setCountryName(c: String): DnBuilder
    fun setStreet(street: String): DnBuilder
    fun build(): DistinguishedName
}
