package com.barco.geax.kcertificate

import java.io.File
import java.security.KeyStore

interface KeyStoreReader {
    fun listAliases(keyStorePath: String, password: CharArray): List<String>
    fun listAliases(keyStoreFile: File, password: CharArray): List<String>
    fun listAliases(keyStore: KeyStore): List<String>
}
