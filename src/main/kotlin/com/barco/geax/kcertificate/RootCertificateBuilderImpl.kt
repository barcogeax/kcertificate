package com.barco.geax.kcertificate

import com.barco.geax.kcertificate.KeysUtil.generateKeyPair
import com.barco.geax.kcertificate.Signer.SignerWithSerial
import com.barco.geax.kcertificate.ext.CrlDistPointExtension
import com.barco.geax.kcertificate.ext.KeyUsageExtension
import org.bouncycastle.asn1.x509.BasicConstraints
import org.bouncycastle.asn1.x509.Extension
import java.security.KeyPair
import java.time.ZonedDateTime

internal class RootCertificateBuilderImpl(subject: DistinguishedName) : RootCertificateBuilder {
    private var crlUri: String? = null
    private val pair: KeyPair = generateKeyPair()
    private val signer: SignerWithSerial = SignerImpl(pair, subject, pair.public, subject)
        .setRandomSerialNumber()

    override fun setNotBefore(notBefore: ZonedDateTime): RootCertificateBuilder {
        signer.setNotBefore(notBefore)
        return this
    }

    override fun setNotAfter(notAfter: ZonedDateTime): RootCertificateBuilder {
        signer.setNotAfter(notAfter)
        return this
    }

    override fun validDuringYears(years: Int): RootCertificateBuilder {
        signer.validDuringYears(years)
        return this
    }

    override fun setCrlUri(crlUri: String?): RootCertificateBuilder {
        this.crlUri = crlUri
        return this
    }

    override fun build(): RootCertificate {
        signer.addExtension(
            KeyUsageExtension.create(
                KeyUsageExtension.KeyUsage.KEY_CERT_SIGN,
                KeyUsageExtension.KeyUsage.CRL_SIGN
            )
        )
        if (crlUri != null) {
            signer.addExtension(CrlDistPointExtension.create(crlUri))
        }

        // This is a CA
        signer.addExtension(Extension.basicConstraints, false, BasicConstraints(true))
        val rootCertificate = signer.sign().x509Certificate
        return RootCertificateImpl(rootCertificate, pair.private)
    }
}
